# Run code static analysis on all files of the repository
echo "Running static analysis..."
go vet ./... || exit 1
staticcheck ./... || exit 1

# Format code with golang conventions
echo "Running check formatting code..."
gofmt -w -s -d . || exit 1

# Check dependencies
echo "Running check dependencies..."
go mod tidy || exit 1
go mod verify || exit 1


# Run tests 
echo "Running tests..."
go test -race -coverprofile=coverage.txt -covermode=atomic ./... || exit 1

# Get coverage test info 
go tool cover -func coverage.txt || exit 1   
