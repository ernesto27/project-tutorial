package services

import (
	"net/http"
	"testing"

	"github.com/jarcoal/httpmock"
)

type mockRequest struct {
	method string
	url    string
}

type mockResponse struct {
	statusCode int
}

type mockPetition struct {
	request  mockRequest
	response mockResponse
}

var apiKey = "someapikey"
var endpoint = "https://stripepayment.com"

func TestStripeCreatePayment(t *testing.T) {
	tests := []struct {
		name      string
		petitions []mockPetition
		want      bool
	}{
		{
			name: "StripeCreatePayment success",
			petitions: []mockPetition{
				{
					request: mockRequest{
						method: http.MethodPost,
						url:    "/createPayment",
					},
					response: mockResponse{
						statusCode: http.StatusOK,
					},
				},
			},
			want: false,
		},
		{
			name: "StripeCreatePayment error",
			petitions: []mockPetition{
				{
					request: mockRequest{
						method: http.MethodPost,
						url:    "/createPayment",
					},
					response: mockResponse{
						statusCode: http.StatusNotFound,
					},
				},
			},
			want: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			httpmock.Activate()
			defer httpmock.DeactivateAndReset()

			for _, petition := range tt.petitions {
				httpmock.RegisterResponder(
					petition.request.method,
					endpoint+petition.request.url,
					httpmock.NewStringResponder(petition.response.statusCode, ""))
			}

			s := Stripe{
				ApiKey:   apiKey,
				Endpoint: endpoint,
			}
			err := s.CreatePayment(10)

			if (err != nil) != tt.want {
				t.Errorf("Stripe.createPayment() error want %v, got %v", tt.want, err)
			}

		})
	}
}
