package services

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
)

// This is a example of Stripe like payment service, is not using the real API
type Stripe struct {
	ApiKey   string
	Endpoint string
}

func (s *Stripe) CreatePayment(amount float64) error {
	data := map[string]float64{
		"amout": amount,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	request, err := http.NewRequest("POST", s.Endpoint+"/createPayment", bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}
	request.Header.Set("Authorization", "Bearer "+s.ApiKey)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	// If status code is different to 200, return an error
	if response.StatusCode != http.StatusOK {
		return errors.New("error creating payment")
	}

	return nil
}
