package business

import "errors"

type Order struct {
	ID         int
	CustomerID int
	Items      []string
}

func (o *Order) CalculateTotal() (int, error) {
	if o.ID == 9999 {
		return 0, errors.New("error calculating total")
	}

	return 10, nil
}
