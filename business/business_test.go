package business

import (
	"testing"
)

func TestBusinessLogic(t *testing.T) {
	type fields struct {
		ID int
	}

	tests := []struct {
		name    string
		fields  fields
		wantErr bool
		Total   int
	}{
		{
			name: "Business order calculateTotal success",
			fields: fields{
				ID: 1,
			},
			Total:   10,
			wantErr: false,
		},
		{
			name: "Business order calculateTotal error",
			fields: fields{
				ID: 9999,
			},
			Total:   0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			order := &Order{
				ID: tt.fields.ID,
			}
			total, err := order.CalculateTotal()

			// Check for error
			if tt.wantErr && err == nil {
				t.Fatalf("CalculateTotal() did not return an error, want error")
			} else if !tt.wantErr && err != nil {
				t.Fatalf("CalculateTotal() returned an unexpected error: %v", err)
			}

			// Check for total
			if total != tt.Total {
				t.Fatalf("CalculateTotal() returned an unexpected total: want %v, got %v", tt.Total, total)
			}

		})
	}
}
