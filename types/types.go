package types

type User struct {
	ID    int    `firestore:"id"`
	Name  string `firestore:"name"`
	Email string `firestore:"email"`
}

type BodyParams struct {
	Name  string
	Email string
}

type ResponseAPI struct {
	Status  string
	Message string
}
