package interfaces

import "gitlab.com/ernesto27/project-tutorial/types"

type MyDB interface {
	CreateUser(user types.User) error
	Close()
}
