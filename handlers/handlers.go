package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/ernesto27/project-tutorial/interfaces"
	"gitlab.com/ernesto27/project-tutorial/types"
)

func CreateUser(c *gin.Context, db interfaces.MyDB) {
	// Get the body params
	var bodyParams types.BodyParams
	if err := c.ShouldBindJSON(&bodyParams); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "error al intentar los parametros del body",
		})
		return
	}

	user := types.User{
		Name:  bodyParams.Name,
		Email: bodyParams.Email,
	}

	// Try to create a user on DB, if error return message
	err := db.CreateUser(user)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  "error",
			"message": "error al intentar crear el usuario",
		})
		return
	}

	// Return success message
	c.JSON(http.StatusOK, gin.H{
		"status":  "ok",
		"message": "Usuario creado",
	})

}
