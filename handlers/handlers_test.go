package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/ernesto27/project-tutorial/db"
	"gitlab.com/ernesto27/project-tutorial/interfaces"
	"gitlab.com/ernesto27/project-tutorial/types"
)

func TestCreateUser(t *testing.T) {
	gin.SetMode(gin.TestMode)

	type args struct {
		params interface{}
		db     interfaces.MyDB
	}

	type result struct {
		httpStatus int
		response   types.ResponseAPI
	}

	tests := []struct {
		name     string
		args     args
		expected result
	}{
		{
			name: "Create User Success",
			args: args{
				params: &types.BodyParams{
					Name:  "testname",
					Email: "test@email.com",
				},
				db: &db.Mock{},
			},
			expected: result{
				httpStatus: http.StatusOK,
				response: types.ResponseAPI{
					Status:  "ok",
					Message: "Usuario creado",
				},
			},
		},
		{
			name: "Create User Error",
			args: args{
				params: &types.BodyParams{
					Name:  "testnameerror",
					Email: "test@email.com",
				},
				db: &db.Mock{},
			},
			expected: result{
				httpStatus: http.StatusBadRequest,
				response: types.ResponseAPI{
					Status:  "error",
					Message: "error al intentar crear el usuario",
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			w := httptest.NewRecorder()
			c, _ := gin.CreateTestContext(w)
			reqBody, _ := json.Marshal(tt.args.params)
			c.Request, _ = http.NewRequest(http.MethodPost, "/api/users/create", bytes.NewBuffer(reqBody))

			CreateUser(c, tt.args.db)

			if w.Code != tt.expected.httpStatus {
				t.Errorf("Expected status code %d, got %d", tt.expected.httpStatus, w.Code)
			}

			var resBody types.ResponseAPI
			json.Unmarshal(w.Body.Bytes(), &resBody)
			if resBody.Status != tt.expected.response.Status {
				t.Errorf("Expected body status %s, got %s", tt.expected.response.Status, resBody.Status)
			}
			if resBody.Message != tt.expected.response.Message {
				t.Errorf("Expected body message %s, got %s", tt.expected.response.Message, resBody.Message)
			}
		})
	}
}
