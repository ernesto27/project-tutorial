# project-tutorial


# Installation
Create .env file

``` bash
cp .env.example .env
```

Run service

``` bash
go run .
```

#

### Mysql setup
Start mysql container

``` bash
docker run -p 3306:3306 --name my-mysql -e MYSQL_ROOT_PASSWORD=mypassword -d mysql:8
```
Update .env with necessary values

#

### Firebase setup
Download firebase configuration file and place it in the repository root with the name firestore.json.

It can be obtained from the Google Firebase dashboard.
Overview -> Project settings -> Service accounts -> Admin SDK -> Generate new private key

#

### Pre-commits hook configuration
Copy file to git hooks folder

``` bash
cp scripts/pre-commit.sh .git/hooks/pre-commit
```

this script will execute when you try to commit changes.

