package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/ernesto27/project-tutorial/db"
	"gitlab.com/ernesto27/project-tutorial/handlers"
	"gitlab.com/ernesto27/project-tutorial/interfaces"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// Background tasks
	go backupDB()
	go sendEmails()

	DBType := os.Getenv("DB_TYPE")
	firestoreFile := os.Getenv("FIRESTORE_FILE")
	mysqlHost := os.Getenv("MYSQL_HOST")
	mysqlUser := os.Getenv("MYSQL_USER")
	mysqlPassword := os.Getenv("MYSQL_PASSWORD")
	mysqlPort := os.Getenv("MYSQL_PORT")
	mysqlDatabase := os.Getenv("MYSQL_DATABASE")

	db := getDBType(DBType, firestoreFile, mysqlHost, mysqlUser,
		mysqlPassword, mysqlPort, mysqlDatabase)
	defer db.Close()

	r := gin.Default()
	r.GET("api/version", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"version": "1.0.1",
		})
	})

	r.POST("api/users/create", func(c *gin.Context) {
		handlers.CreateUser(c, db)
	})
	r.Run(":" + os.Getenv("PORT"))
}

func getDBType(DBType, firestoreFile, mysqlHost, mysqlUser,
	mysqlPassword, mysqlPort, mysqlDatabase string) interfaces.MyDB {

	switch DBType {
	case "firestore":
		f, err := db.NewFirebase(firestoreFile)
		if err != nil {
			panic(err)
		}
		return f
	case "mysql":
		m, err := db.NewMysql(mysqlHost, mysqlUser, mysqlPassword, mysqlPort, mysqlDatabase)
		if err != nil {
			panic(err)
		}
		return m
	case "mock":
		return &db.Mock{}
	default:
		panic("DB_TYPE not found")
	}

}

func backupDB() {
	for {
		// this code runs every hour
		fmt.Println("doing backup db job...")

		// wait for an hour on a new goroutine
		time.Sleep(time.Hour)
	}
}

func sendEmails() {
	for {
		// this code runs every hour
		fmt.Println("send emails job...")

		// wait for an hour on a new goroutine
		time.Sleep(time.Hour)
	}
}
