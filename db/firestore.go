package db

import (
	"context"
	"log"

	"cloud.google.com/go/firestore"
	firebase "firebase.google.com/go"
	"gitlab.com/ernesto27/project-tutorial/types"
	"google.golang.org/api/option"
)

type Firebase struct {
	app    *firebase.App
	client *firestore.Client
}

func NewFirebase(credentialsFile string) (*Firebase, error) {
	opt := option.WithCredentialsFile(credentialsFile)

	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}

	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, err
	}

	return &Firebase{
		app:    app,
		client: client,
	}, nil
}

func (f *Firebase) CreateUser(user types.User) error {
	_, _, err := f.client.Collection("users").Add(context.Background(), user)
	return err
}

func (m *Firebase) Close() {}
