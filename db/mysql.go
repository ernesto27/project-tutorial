package db

import (
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/ernesto27/project-tutorial/types"

	_ "github.com/go-sql-driver/mysql"
)

type Mysql struct {
	db *sql.DB
}

func NewMysql(host, user, password, port, database string) (*Mysql, error) {
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, password, host, port, database))
	if err != nil {
		return nil, err
	}

	// Test the connection
	err = db.Ping()
	if err != nil {
		return nil, errors.New("error connecting to the database")
	}

	return &Mysql{
		db: db,
	}, nil
}

func (m *Mysql) CreateUser(user types.User) error {
	// create query save user
	query := "INSERT INTO users (name, email) VALUES (?, ?)"
	// prepare the query
	stmt, err := m.db.Prepare(query)
	if err != nil {
		return err
	}
	// close the statement
	defer stmt.Close()

	// execute the query
	_, err = stmt.Exec(user.Name, user.Email)
	if err != nil {
		return err
	}

	return nil
}

func (m *Mysql) Close() {
	m.db.Close()
}
