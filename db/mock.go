package db

import (
	"errors"

	"gitlab.com/ernesto27/project-tutorial/types"
)

type Mock struct{}

func (m *Mock) CreateUser(user types.User) error {
	if user.Name == "testnameerror" {
		return errors.New("error al intentar crear el usuario")
	}

	return nil
}

func (m *Mock) Close() {}
